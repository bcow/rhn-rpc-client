package org.patentpending.redhat.rhn;

import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

/**
 * XML-RPC wrapper around Red Hat Network API.
 * 
 * @author Antti Peltonen <antti.peltonen@iki.fi>
 *
 */
public class RpcClient
{
    /*
     * Apache XML-RPC client object
     */
    private XmlRpcClient satelliteRPCClient;

    /*
     * Session token for active connection to RHN
     */
    private String session;

    private final Logger logger = Logger.getLogger(RpcClient.class);
    
    private URL satelliteAPIURL = null;
    
    /**
     * Return the URL object used to connect to RHN server
     * 
     * @return URL object of RHN or Satellite server
     */
    public URL getURL()
    {
        return satelliteAPIURL;
    }

    /**
     * Check if user is logged on to RHN api connection
     * 
     * @return True if connection is established and session token has been received.
     */
    public boolean isLoggedOn()
    {
	if (session != null)
	    return true;
	else
	    return false;
    }
    
    
    /**
     * List errata applicable to a channel
     * 
     * @param label Channel label
     * @return List of HashMap objects with Errata info
     * @throws ProtocolException When user is not logged in to the session yet.
     * @return HashMap representation of the channel details
     */
    public List listErrata(String label) throws ProtocolException, XmlRpcException
    {
        logger.debug("RpcClient::listErrata(String channelLabel)");
	if (!isLoggedOn())
	    throw new ProtocolException("No session token available.");
        ArrayList errata = new ArrayList();
        Object[] params = new Object[]{ session, label };
        Object[] response =  (Object[]) satelliteRPCClient.execute("channel.software.listErrata", params);
	for (Object row: response)
	    errata.add((HashMap) row);
        return errata;
    }

    /**
     * Clone given errata's as original to destination channel
     * 
     * @param channelLabel Channel into which clone an errata
     * @param advisories Array of Strings that contain advisory name like (FUBAR-2000:123)
     * @return ArrayList of HashMap objects containing new names etc for the Erratas
     * @throws ProtocolException When user is not logged in to the session yet.
     * @return HashMap representation of the channel details
     */
    public List cloneErrataAsOriginal(String channelLabel, String[] advisories) throws ProtocolException, XmlRpcException
    {
        logger.debug("RpcClient::cloneErrataAsOriginal(String channelLabel, String [] advisories)");
	if (!isLoggedOn())
	    throw new ProtocolException("No session token available.");
        ArrayList errata = new ArrayList();
        Object[] params = new Object[]{ session, channelLabel, advisories };
        Object[] response =  (Object[]) satelliteRPCClient.execute("errata.cloneAsOriginal", params);
	for (Object row: response)
	    errata.add((HashMap) row);
        return errata;
    }
    
    /**
     * List errata applicable to a channel based on advisory type
     * 
     * @param label Channel label
     * @param type Advisory type (one of of the following: 'Security Advisory', 'Product Enhancement Advisory', 'Bug Fix Advisory')
     * @return List of HashMap objects with Errata info
     * @throws ProtocolException When user is not logged in to the session yet.
     * @return HashMap representation of the channel details
     */
    public List listErrataByType(String label, String type) throws ProtocolException, XmlRpcException
    {
        logger.debug("RpcClient::listErrataByType(String channelLabel)");
	if (!isLoggedOn())
	    throw new ProtocolException("No session token available.");
        ArrayList errata = new ArrayList();
        Object[] params = new Object[]{ session, label, type };
        Object[] response =  (Object[]) satelliteRPCClient.execute("channel.software.listErrataByType", params);
	for (Object row: response)
	    errata.add((HashMap) row);
        return errata;
    }

    /**
     * Return channel information as a hashmap
     * 
     * @throws XmlRpcException When communication with RHN fails.
     * @throws ProtocolException When user is not logged in to the session yet.
     * @return HashMap representation of the channel details
     */
    public HashMap getChannelDetails(String channelLabel) throws ProtocolException, XmlRpcException
    {
	logger.debug("RpcClient::getChannelDetails(String channelLabel)");
	if (!isLoggedOn())
	    throw new ProtocolException("No session token available.");
	Object[] params = new Object[]{ session, channelLabel };
        return (HashMap) satelliteRPCClient.execute("channel.software.getDetails", params);
    }
    
    /**
     * Get custom value fields from system profile
     * 
     * @param sid System identification number
     * @return HashMap of custom values in systems profile
     * @throws XmlRpcException When communication with RHN fails.
     * @throws ProtocolException When user is not logged in to the session yet.
     */
    @SuppressWarnings("rawtypes")
    public HashMap getCustomValues(int sid) throws XmlRpcException, ProtocolException
    {
	logger.debug("RpcClient::getCustomValues(int sid)");
	if (!isLoggedOn())
	    throw new ProtocolException("No session token available.");
	Object[] params = new Object[]{ session, sid };
	return (HashMap) satelliteRPCClient.execute("system.getCustomValues", params);		
    }

    /**
     * List all channels visible for a user
     * 
     * @return HashMap of channels available for user
     * @throws ProtocolException If user is not logged on
     * @throws XmlRpcException  If there is an protocol error while communicating with server
     */
    @SuppressWarnings("rawtypes")
    public List listAllChannels() throws ProtocolException, XmlRpcException
    {
	logger.debug("RpcClient::listAllChannels()");
	if (!isLoggedOn())
	    throw new ProtocolException("No session token available.");
        ArrayList channels = new ArrayList();
        Object[] params = new Object[]{ session };
        Object[] response =  (Object[]) satelliteRPCClient.execute("channel.listAllChannels", params);
	for (Object row: response)
	    channels.add((HashMap) row);
	return channels;
    }

    /**
     * List all Red Hat provided channels visible for a user
     * 
     * @return HashMap of channels available for user
     * @throws ProtocolException If user is not logged on
     * @throws XmlRpcException  If there is an protocol error while communicating with server
     */
    @SuppressWarnings("rawtypes")
    public List listRedHatChannels() throws ProtocolException, XmlRpcException
    {
	logger.debug("RpcClient::listRedHatChannels()");
	if (!isLoggedOn())
	    throw new ProtocolException("No session token available.");
        ArrayList channels = new ArrayList();
        Object[] params = new Object[]{ session };
        Object[] response =  (Object[]) satelliteRPCClient.execute("channel.listRedHatChannels", params);
	for (Object row: response)
	    channels.add((HashMap) row);
	return channels;
    }
    
    /**
     * Gets system info from the server based on the sid
     * 
     * @param sid System identification number
     * @return HashMap of system info values
     * @throws XmlRpcException When communication with RHN fails
     * @throws ProtocolException When user is not logged in to the session yet.
     */
    @SuppressWarnings("rawtypes")
    public HashMap getSystemDetails(int sid) throws XmlRpcException, ProtocolException
    {
	logger.debug("RpcClient::getSystemDetails(int sid)");
	if (!isLoggedOn())
	    throw new ProtocolException("No session token available.");
	Object[] params = new Object[]{ session, sid };
	return (HashMap) satelliteRPCClient.execute("system.getDetails", params);
    }
	
    /**
     * List systems seen by user account
     * 
     * @return List of HashMap objects containing information about the system profiles seen by users account
     * @throws XmlRpcException When communication with RHN fails
     * @throws ProtocolException When user is not logged in to the session yet.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public List listSystems() throws XmlRpcException, ProtocolException
    {
	logger.debug("RpcClient::listSystems()");
	if (!isLoggedOn())
	    throw new ProtocolException("No session token available.");
	ArrayList systems = new ArrayList();
	Object[] params = new Object[]{ session };
	Object[] response = (Object[]) satelliteRPCClient.execute("system.listUserSystems", params);
	for (Object row: response)
	    systems.add((HashMap) row);
	return systems;
    }

    /**
     * Login to RPC API and receive session token that needs to be transmitted on every query to succeed
     * 
     * @param username Username of RHN account
     * @param password Password for the RHN user account.
     * @throws XmlRpcException When communication with RHN fails
     * @throws ProtocolException When user is already logged on.
     */
    public void loginRPC(String username, String password) throws XmlRpcException, ProtocolException
    {
	logger.debug("RpcClient::loginRPC(username password)");
	if (isLoggedOn())
	    throw new ProtocolException("User already logged in.");
	Object[] params = new Object[]{ username, password };
	session = (String) satelliteRPCClient.execute("auth.login", params);
    }

    /**
     * Initialize RPC object and connection to RHN
     * 
     * @param hostname Hostname of RHN server, Satellite or Proxy
     * @param installAllPermittingHostVerifyier If true, use custom all permitting HostVerifier that bypasses all SSL verification
     * @throws MalformedURLException Hostname of the RHN server was invalid.
     * @throws XmlRpcException Communication with the server did not succeed.
     * @throws NoSuchAlgorithmException Security algorithm was not available.
     * @throws KeyManagementException Unable to handle crypto keys.
     * @throws ProtocolException User was already logged in.
     */
    public void initRPC(String hostname, boolean installAllPermittingHostVerifyier) throws MalformedURLException, XmlRpcException, NoSuchAlgorithmException, KeyManagementException, ProtocolException
    {
	logger.debug("RpcClient::initRPC(hostname)");
	if (isLoggedOn())
	    throw new ProtocolException("User already logged in.");

	satelliteAPIURL = new URL("https://" + hostname + "/rpc/api");
	logger.debug("Red Hat Network server URL: " +satelliteAPIURL.toString());
	
	/*
	 * Create Trust manager that always trusts every X509 certificate from SSL URL
	 */
        TrustManager[] trustAllCerts = new TrustManager[]
        {
    	new X509TrustManager()
    	{
    	    private final Logger logger = Logger.getLogger(X509TrustManager.class);
    	    public X509Certificate[] getAcceptedIssuers()
    	    {
    		logger.warn("This promiscious built-in X509TrustManager has no accepted issuers.");
    		return null;
    	    }	 
    	    public void checkClientTrusted(X509Certificate[] certs, String authType)
    	    {
    		// Trust always
    		logger.warn("This promiscious built-in X509TrustManager trusts all clients.");
    	    }	 
    	    public void checkServerTrusted(X509Certificate[] certs, String authType)
    	    {
    		// Trust always
    		logger.warn("This promiscious built-in X509TrustManager trusts all servers.");
    	    }
    	}
        };
		
        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        // Create empty HostnameVerifier
        HostnameVerifier hv = new HostnameVerifier()
        {
            public boolean verify(String arg0, SSLSession arg1)
            {
        	logger.warn("This promiscious HostnameVerifier accepts all hostnames.");
        	return true;
            }
        };

        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        if (installAllPermittingHostVerifyier)
            HttpsURLConnection.setDefaultHostnameVerifier(hv);
        XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
	    
        config.setServerURL(satelliteAPIURL);
        config.setEnabledForExtensions(true);
        satelliteRPCClient = new XmlRpcClient();
        satelliteRPCClient.setConfig(config);
    }

    /**
     * Get API and RHN versions from remote server.
     * 
     * @return HashMap (api, rhn) of all version information available.
     * @throws XmlRpcException Communication with the server did not succeed.
     */
    public HashMap<String, String> getVersions() throws XmlRpcException
    {
	logger.debug("RpcClient::getVersions()");
	Object response = (Object) satelliteRPCClient.execute("api.getVersion", (Object[]) null);
	Object response2 = (Object) satelliteRPCClient.execute("api.systemVersion", (Object[]) null);
	HashMap<String, String> result = new HashMap<String, String>();    
	result.put("api", (String) response);
	result.put("rhn", (String) response2);
	return result;
    }

    /**
     * Gets system DMI / hardware info for the server based on the sid
     * 
     * @param sid System identification number in RHN
     * @return HashMap of DMI information from system profile
     * @throws XmlRpcException When communication with the server fails.
     */
    @SuppressWarnings("rawtypes")
    public HashMap getDmi(int sid) throws XmlRpcException
    {
	logger.debug("RpcClient::getDmi(int sid)");
	Object[] params = new Object[]{ session, sid };
	return (HashMap) satelliteRPCClient.execute("system.getDmi", params);
    }

    /**
     * Gets system CPU info for the server based on the sid
     * 
     * @param sid System identification number in RHN
     * @return HashMap of CPU information from system profile
     * @throws XmlRpcException When communication with the server fails.
     */
    @SuppressWarnings("rawtypes")
    public HashMap getCpu(int sid) throws XmlRpcException
    {
	logger.debug("RpcClient::getCpu(int sid)");
	Object[] params = new Object[]{ session, sid };
	return (HashMap) satelliteRPCClient.execute("system.getCpu", params);
    }
}
